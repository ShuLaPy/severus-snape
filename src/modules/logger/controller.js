const elasticCo = require('../../libs/elasticCo');
const opensearch = require('../../libs/opensearch');

exports.init = async({node, username, password, service}) => {
  switch (service) {
    case 'opensearch':
      opensearch.init({ node, username, password });
      break;
    case 'elastic-co':
      elasticCo.init({ node, username, password });
      break;
  }
  return {
    status: 200,
    success: true,
  };
}

exports.add = async({ index, payload, platform, service }) => {
  let response;
  switch (service) {
    case 'opensearch':
      response = await opensearch.addRecord({ ...payload, platform }, index);
      break;
    case 'elastic-co':
      response = await elasticCo.addRecord({ ...payload, platform }, index);
      break;
  }
  return response;
};

exports.update = async({ index, query, script, service }) => {
  switch (service) {
    case 'opensearch':
      await opensearch.updateByQuery(query, script, index);
      break;
    case 'elastic-co':
      await elasticCo.updateByQuery(query, script, index);
      break;
  }

  return {
    status: 200,
    success: true,
  };
};

exports.indexExists = async({index, service}) =>{
  let res;
  switch (service) {
    case 'opensearch':
      res = await opensearch.indexExist(index);
      break;
    case 'elastic-co':
      res = await elasticCo.indexExist(index);
      break;
  }

  return res;
}

exports.search = async({ index, query, size, service }) => {
  let data;
  switch (service) {
    case 'opensearch':
      data = await opensearch.search(query, index, size);
      break;
    case 'elastic-co':
      data = await elasticCo.search(query, index, size);
      break;
  }

  return { data };
};

exports.getIndex = ({ baseIndex, service, date, env }) => {
  switch (service) {
    case 'opensearch':
      return opensearch.getIndex(baseIndex, date, env);
    case 'elastic-co':
      return elasticCo.getIndex(baseIndex, date, env);
  }
};

exports.getOrderAnalytics = ({ payload, service }) => {
  let queryResult;
  switch (service) {
    case 'elastic-co':
      queryResult = elasticCo.getOrderAnalytics(payload);
      break;
    case 'opensearch': 
      queryResult = opensearch.getOrderAnalytics(payload);
      break;
  }

  return {
    status: 200,
    success: true,
    data: {
      queryResult,
    },
    message: 'Fetched Result',
  };
};

exports.getOrderTopCategories = ({ payload, service }) => {
  let queryResult;
  switch (service) {
    case 'elastic-co':
      queryResult = elasticCo.getOrderTopCategories(payload);
      break;
    case 'opensearch':
        queryResult = opensearch.getOrderTopCategories(payload)
        break;
  }

  return {
    status: 200,
    success: true,
    data: {
      queryResult,
    },
    message: 'Fetched Result',
  };
};

exports.getOrderTopTransactions = async({ payload, service }) => {
  let queryResult;
  switch (service) {
    case 'elastic-co':
      queryResult = await elasticCo.getOrderTopTransactions(payload);
      break;
    case 'opensearch':
      queryResult = await elasticCo.getOrderTopTransactions(payload)
      break;
  }

  return {
    status: 200,
    success: true,
    data: {
      queryResult,
    },
    message: 'Fetched Result',
  };
};

exports.getOrderRevenueTrend = async({ payload, service }) => {
  let queryResult;
  switch (service) {
    case 'elastic-co':
      queryResult = await elasticCo.getOrderRevenueTrend(payload);
      break;
    case 'opensearch':
      queryResult = await opensearch.getOrderRevenueTrend(payload)
      break;
  }

  return {
    status: 200,
    success: true,
    data: {
      queryResult,
    },
    message: 'Fetched Result',
  };
};

exports.getOrderRealTimeSales = async({ payload, service }) => {
  let queryResult;
  switch (service) {
    case 'elastic-co':
      queryResult = await elasticCo.getOrderRealTimeSales(payload);
      break;
    case 'opensearch': 
      queryResult = await opensearch.getOrderRealTimeSales(payload)
      break;
  }

  return {
    status: 200,
    success: true,
    data: {
      queryResult,
    },
    message: 'Fetched Result',
  };
};

exports.getOrderInstantSales = async({ payload, service }) => {
  let queryResult;
  switch (service) {
    case 'elastic-co':
      queryResult = await elasticCo.getInstantTimeSales(payload);
      break;
    case 'opensearch': 
      queryResult = await opensearch.getInstantTimeSales(payload)
      break;
  }

  return {
    status: 200,
    success: true,
    data: {
      queryResult,
    },
    message: 'Fetched Result',
  };
};

exports.getWebcheckinAnalytics = async({ payload, service }) => {
  let queryResult;
  switch (service) {
    case 'elastic-co':
      queryResult = await elasticCo.getWebcheckinAnalytics(payload);
      break;
    case 'opensearch': 
      queryResult = await opensearch.getWebcheckinAnalytics(payload)
      break;
  }

  return {
    status: 200,
    success: true,
    data: {
      queryResult,
    },
    message: 'Fetched Result',
  };
}

exports.getCheckinWindow = async({ payload, service }) => {
  let queryResult;
  switch (service) {
    case 'elastic-co':
      queryResult = await elasticCo.getCheckinWindow(payload);
      break;
    case 'opensearch': 
      queryResult = await opensearch.getCheckinWindow(payload)
      break;
  }

  return {
    status: 200,
    success: true,
    data: {
      queryResult,
    },
    message: 'Fetched Result',
  };
}

exports.getCheckinTrends = async ({ payload, service }) => {
  let queryResult;
  switch (service) {
    case 'elastic-co':
      queryResult = await elasticCo.getCheckinTrend(payload);
      break;
    case 'opensearch': 
      queryResult = await opensearch.getCheckinTrend(payload)
      break;
  }

  return {
    status: 200,
    success: true,
    data: {
      queryResult,
    },
    message: 'Fetched Result',
  };
}