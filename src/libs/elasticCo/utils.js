exports.getCalenderInterval = (interval) => {
    switch(interval){
        case 'minute': 
            return '1m';
        case 'day': 
            return '1d';
        case 'hour': 
            return '1h';
        case 'week':
            return '1w';
        case 'month':
            return '1M';
    }
}