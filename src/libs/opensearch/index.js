const { Client } = require('@opensearch-project/opensearch');
const { padding } = require('../../utils/padding');
var client;

const getByQuery = async ({ index, query, sort, size, aggs }) => {
  const payload = {
    index,
    size,
    query: query && query,
    sort: sort && sort,
    aggs: aggs && aggs,
  };
  try {
    const res = await client.search(payload);
    return res;
  } catch (err) {
    console.log(err);
  }
};

const createIndex = async (index) => {
  try {
    await client.indices.create({
      index,
      body: {
        settings: {
          index: {
            number_of_shards: 1,
            number_of_replicas: 1,
          },
        },
      },
    });
  } catch (error) {
    console.log('osCreateIndexError:', error);
  }
};

const addToOS = async (data, index) => {
  try {
    const response = await client.index({
      index: index,
      body: { ...data },
    });
    return response;
  } catch (error) {
    throw error;
  }
};

exports.init = ({node, username, password}) => {
  client = new Client({
    node,
    auth: {
      username,
      password,
    },
  });
}

exports.indexExist = async(index) => {
  const indexExists = await client.indices.exists({
    index,
  });
  return indexExists.body
}

exports.addRecord = async (data, index) => {
  try {
    const indexExists = await client.indices.exists({
      index,
    });
    if (!indexExists) {
      await createIndex(index);
    }
    const res = await addToOS(data, index);
    return res?.body;
  } catch (error) {
    console.log('osAddRecordError:', error);
  }
};

exports.getIndex = (baseIndex, date, env) => {
  return `${env}-${baseIndex}-${date.getFullYear()}.${padding(
    date.getMonth() + 1
  )}`;
};

exports.getRecords = async (index) => {
  try {
    const result = await client.search({
      index: index,
    });
    return result;
  } catch (error) {
    throw error;
  }
};

exports.search = async(query, index, size) => {
  const res = await client.search({
    index,
    body: { query },
  });

  console.log(res)

  return res.body.hits;
}

exports.esCount = async (index, query) => {
  try {
    const { body } = await client.count({
      index,
      body: {
        query,
      },
    });
    return body.count || 0;
  } catch (error) {
    throw error;
  }
};

exports.addBulk = async (dataset, index) => {
  const indexExist = await client.indices.exists({
    index,
  });
  if (!indexExist.body) {
    await createIndex(index);
  }
  const body = dataset.flatMap((doc) => [{ index: { _index: index } }, doc]);
  await client.bulk({ refresh: true, body });
};

exports.updateByQuery = async (query, script, index) => {
  await client.updateByQuery({
    index,
    body: {
      query,
      script,
    },
  });
};

exports.getOrderAnalytics = async (payload) => {
  const res = await getByQuery({
    index: payload.index,
    query: {
      bool: {
        must: [
          { match: payload.matchObject },
          {
            bool: {
              should: payload.acceptedStatus.map((ele) => {
                return {
                  term: {
                    status: ele,
                  },
                };
              }),
            },
          },
          {
            range: {
              createdAt: {
                gte: payload.fromDate,
                lte: payload.toDate,
              },
            },
          },
        ],
      },
    },
    aggs: {
      totalRequests: {
        value_count: {
          field: 'totalPrice',
        },
      },
      totalRevenue: {
        sum: {
          field: 'totalPrice',
        },
      },
      refund: {
        filter: { term: { status: 'refund' } },
      },
    }
  });

  return res;
}

exports.getOrderTopCategories = async(payload) => {
  const res = await getByQuery({
    index: payload.size,
    query: {
      bool: {
        must: [
          { match: payload.matchObject },
          {
            bool: {
              should: payload.acceptedStatus.map((ele) => {
                return { term: { status: ele } };
              }),
            },
          },
          {
            range: {
              createdAt: {
                gte: payload.fromDate,
                lte: payload.toDate,
              },
            },
          },
        ],
      },
    },
    aggs: {
      aggregation: {
        terms: { field: payload.aggregationTerm },
      },
    },
    size: payload.size,
  });

  return res;
}

exports.getOrderTopTransactions = async (payload) => {
  const res = await getByQuery({
    index: payload.index,
    query: {
      bool: {
        must: [
          { match: payload.matchObject },
          {
            bool: {
              should: payload.acceptedStatus.map((ele) => {
                return { term: { status: ele } };
              }),
            },
          },
          {
            range: {
              createdAt: {
                gte: from,
                lte: to,
              },
            },
          },
        ],
      },
    },
    sort: [
      {
        totalPrice: {
          order: payload.sortOrder,
        },
      },
    ],
    size: payload.size,
  });

  return res;
};

exports.getOrderRevenueTrend = async (payload) => {
  const res = await getByQuery({
    index: payload.index,
    query: {
      bool: {
        must: [
          { match: payload.matchObject },
          {
            bool: {
              should: payload.acceptedStatus.map((ele) => {
                return { term: { status: ele } };
              }),
            },
          },
          {
            range: {
              createdAt: {
                gte: payload.fromDate,
                lte: payload.toDate,
              },
            },
          },
        ],
      },
    },
    aggs: {
      trends: {
        date_histogram: {
          field: payload.dateHistogramField,
          calendar_interval: payload.calenderInterval,
          offset: payload.offset,
          min_doc_count: 0,
          time_zone: payload.timezone,
          extended_bounds: {
            max: new Date(payload.toDate).getTime(),
            min: new Date(payload.fromDate).getTime(),
          },
        },
        aggs: {
          totalRevenue: {
            sum: {
              field: payload.dateHistogramSumField,
            },
          },
        },
      },
    },
  });

  return res;
};

exports.getOrderRealTimeSales = async (payload) => {
  const res = await getByQuery({
    index: payload.index,
    query: {
      bool: {
        must: [
          {
            match: payload.matchObject,
          },
          {
            range: {
              createdAt: {
                gte: `now-${payload.tenure}`,
              },
            },
          },
          {
            bool: {
              should: payload.acceptedStatus.map((ele) => {
                return { term: { status: ele } };
              }),
            },
          },
        ],
      },
    },
    size:  payload.size,
    aggs: {
      per_min: {
        date_histogram: {
          field: payload.dateHistogramField,
          calendar_interval: payload.calenderInterval,
          min_doc_count: 0,
          extended_bounds: {
            max: 'now/m',
            min: `now-${payload.tenure}`,
          },
        },
        aggs: {
          top30: {
            bucket_sort: {
              sort: [],
              size: payload.size,
            },
          },
        },
      },
    },
  });

  return res;
};

exports.getInstantTimeSales = async(payload) => {
  const res = await getByQuery({
    index: payload.index,
    query: {
      bool: {
        must: [
          {
            match: payload.matchObject,
          },
          {
            range: {
              createdAt: {
                gte: `now-${payload.tenure}`,
              },
            },
          },
          {
            bool: {
              should: payload.acceptedStatus.map((ele) => {
                return { term: { status: ele } };
              }),
            },
          },
        ],
      },
    },
    aggs: {
      per_min: {
        date_histogram: {
          field: payload.dateHistogramField,
          calendar_interval: getCalenderInterval(payload.interval),
          min_doc_count: 0,
          extended_bounds: {
            max: 'now/m',
            min: `now-${payload.tenure}`,
          },
          order: {
            _key: payload.dateHistogramSort,
          },
        },
        aggs: {
          top30: {
            bucket_sort: {
              sort: [],
              size: payload.size,
            },
          },
        },
      },
    },
  });

  return res;
}

exports.getWebcheckinAnalytics = async(payload) => {
  const res = await getByQuery({
    index: payload.index,
    query: {
      bool: {
        must: [
          { match: payload.matchObject },
          {
            range: {
              createdAt: {
                gte: payload.fromDate,
                lte: payload.toDate,
              },
            },
          },
        ],
      },
    },
    aggs: {
      checkins: {
        filter: {
          exists: {
            field: payload.filterExistsField,
          },
        },
      },
    },
    size: payload.size,
  });

  return res;
}

exports.getCheckinWindow = async (payload) => {
  const res = await getByQuery({
    index: payload.index,
    query: {
      bool: {
        must: [
          { match: payload.matchObject },
          { exists: payload.existsObject },
          {
            range: {
              createdAt: {
                gte: payload.fromDate,
                lte: payload.toDate,
              },
            },
          },
        ],
      },
    },
    aggs: {
      grouping: {
        terms: {
          script: `
          ZonedDateTime d1 = doc['checkin'].value;
          ZonedDateTime d2 = doc['webcheckin'].value;
          long differenceInMillis = ChronoUnit.MILLIS.between(d1, d2);
          double dif = Math.abs(differenceInMillis/86400000);
          if(dif<=1.0){
            return 'firstLot';
          }else if(dif<=3.0){
            return 'secondLot';
          }else if(dif<=7.0){
            return 'thirdLot';
          }else{
            return 'fourthLot';
          }`,
        },
      },
    },
    size: payload.size,
  });

  return res;
}

exports.getCheckinTrend = async (payload) => {
  const res = await getByQuery({
    index: payload.index,
    query: {
      bool: {
        must: [
          { match: payload.matchObject },
          { exists: payload.existsObject },
          {
            range: {
              createdAt: {
                gte: payload.fromDate,
                lte: payload.toDate,
              },
            },
          },
        ],
      },
    },
    aggs: {
      trends: {
        date_histogram: {
          field: payload.dateHistogramField,
          calendar_interval: payload.calenderInterval,
          offset: payload.offset,
          min_doc_count: 0,
          time_zone: payload.timeZone,
          extended_bounds: {
            max: new Date(payload.toDate).getTime(),
            min: new Date(payload.fromDate).getTime(),
          },
        },
      },
    },
  });

  return res;
}