# Severus Snape

Severus Snape is a private npm package that provides a wrapper for Elasticsearch and OpenSearch. It allows developers to interact with these search engines in a simplified and efficient manner. This package is designed to be used in private projects.

## Installation

Since Severus Snape is a private npm package, you need to have access to the private repository in order to install and use it. 

1. Make sure you have access to the private npm registry where the package is hosted.
2. Add the package to your `package.json` file's dependencies:

```json
{
  "dependencies": {
    "severus-snape": "1.0.0"
  }
}
```

3. Run npm install to fetch the package and its dependencies.

## Usage

To use Severus Snape in your project, you first need to import it:

```
const SeverusSnape = require('severus-snape');
```

Once imported, you can create an instance of SeverusSnape and start interacting with Elasticsearch and OpenSearch:

```
const client = new SeverusSnape({
  hosts: ['http://localhost:9200', 'http://localhost:5601'],
  username: 'your-username',
  password: 'your-password'
});

// Example: Index a document
const indexDocument = async (index, id, document) => {
  try {
    const response = await client.index({
      index,
      id,
      body: document
    });
    console.log('Document indexed:', response);
  } catch (error) {
    console.error('Failed to index document:', error);
  }
};

// Example: Search for documents
const searchDocuments = async (index, query) => {
  try {
    const response = await client.search({
      index,
      body: {
        query: {
          match: {
            content: query
          }
        }
      }
    });
    console.log('Search results:', response.hits.hits);
  } catch (error) {
    console.error('Failed to search documents:', error);
  }
};

// Example usage
indexDocument('my-index', '1', { content: 'Hello, world!' });
searchDocuments('my-index', 'Hello');
```

Remember to replace 'http://localhost:9200' and 'http://localhost:5601' with the actual URLs of your Elasticsearch and OpenSearch instances, respectively. Also, provide your valid username and password for authentication if required.

## Documentation

Unfortunately, since Severus Snape is a private npm package, the documentation is not publicly available. However, you can refer to the package's source code and inline comments for guidance on its functionality and usage.

## Contributing

Contributions to Severus Snape are not open to the public since it is a private project. However, if you encounter any issues or have suggestions, please reach out to the project owner or maintainers for assistance.

## License

Severus Snape is released under the MIT License. See the [LICENSE](LICENSE) file for details.
